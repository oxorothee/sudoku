package fr.quentin.sudoku.bo;

public class Grille {

    private int taille = 9;
    private int[][] grille = new int[taille][taille];

    {
        for(int ligne = 0; ligne < taille; ligne++){
            for(int colone = 0; colone < taille; colone++){
                this.grille[ligne][colone] = 0;
            }
        }
    }


    public void set(int ligne, int colone, int value){
        this.grille[ligne][colone] = value;
    }

    public int getTaille(){
        return this.taille;
    }

    public int[] getligne(int index){
        return this.grille[index];
    }

    public int[] getColone(int index){
        int[] colone = new int[this.taille];
        for (int pos = 0; pos < this.taille; pos++ ) {
            colone[pos] = this.grille[pos][index];
        }
        return colone;
    }

    public int[] getTable(int ligne, int colone){
        int[] table = new int[this.taille];
        int indexligne = indexTableByPos(ligne);
        int indexColone = indexTableByPos(colone);
        int pos = 0;

        for(int li = indexligne; li < indexligne + 3; li++){
            for(int col = indexColone; col < indexColone + 3; col++){
                table[pos] = this.grille[li][col];
                pos++;
            }
        }
        return table;
    }

    private int indexTableByPos(int pos){
        return switch (pos) {
            case 1, 2, 3 -> 0;
            case 4, 5, 6 -> 3;
            default -> 6;
        };
    }


    public String toString(){
        StringBuilder stringGrille = new StringBuilder();
        for(int ligne = 0; ligne < taille; ligne++){
            for(int colone = 0; colone < taille; colone++){
                stringGrille.append(" " + this.grille[ligne][colone] + " ");
            }
            stringGrille.append("\n");
        }
        return stringGrille.toString();
    }

}
