package fr.quentin.sudoku.bll;

import fr.quentin.sudoku.bo.Grille;

public class GrilleControlleur {

    // class qui controle que les regle du sudoku sont bien respecter

    public static boolean control(Grille grille){
        boolean ligne, colone, table;

        ligne = controlLigne(grille);
        colone = controlColone(grille);
        table = controleTable(grille);

        return ligne && colone && table;
    }


    private static boolean controlLigne(Grille grille) {
        boolean isGood = true;
        for(int i = 0; i < grille.getTaille(); i++){
            // recupere la ligne a controler
            int[] ligne = grille.getligne(i);

            for(int nombreController = 1; nombreController <= grille.getTaille(); nombreController++){
                int total = 0;
                for (int nombreGrille: ligne) {
                    if(nombreGrille == nombreController){
                        total++;
                    }
                    if(total > 1) isGood = false;
                }
            }
        }

        return isGood;
    }

    private static boolean controlColone(Grille grille) {
        boolean isGood = true;

        return isGood;
    }

    private static boolean controleTable(Grille grille) {
        boolean isGood = true;

        return isGood;
    }
}
